package com.orhanobut.hawk.Encryption;

import java.nio.charset.Charset;

/**
 * 加密算法
 */
public class RC4 {
    /**
     * 加密
     * @param data byte[]加密的数据
     * @param key key 加密的密令
     * @return String
     */
    public static String decryRc4(byte[] data, String key) {
        if (data == null || key == null) {
            return null;
        }
        return asString(rc4Base(data, key));
    }
    /**
     * 加密
     * @param data 加密的数据
     * @param key key 加密的密令
     * @return String
     */
    public static String decryRc4(String data, String key) {
        if (data == null || key == null) {
            return null;
        }
        return new String(rc4Base(hexString2Bytes(data), key));
    }
    /**
     * 解密
     * @param data 解密的数据
     * @param key key 加密的密令
     * @return byte
     */
    public static byte[] encryRc4Byte(String data, String key) {
        if (data == null || key == null) {
            return null;
        }
        byte[] bData = data.getBytes(Charset.defaultCharset());
        return rc4Base(bData, key);
    }
    /**
     * 解密
     * @param data 解密的数据
     * @param key key 加密的密令
     * @return String
     */
    public static String encryRc4String(String data, String key) {
        if (data == null || key == null) {
            return null;
        }
        return toHexString(asString(encryRc4Byte(data, key)));
    }

    /**
     * 转换
     * @param buf buf
     * @return String
     */
    private static String asString(byte[] buf) {
        StringBuffer strbuf = new StringBuffer(buf.length);
        for (int i = 0; i < buf.length; i++) {
            strbuf.append((char) buf[i]);
        }
        return strbuf.toString();
    }

    private static byte[] initKey(String aKey) {
        byte[] bKey = aKey.getBytes(Charset.defaultCharset());
        byte[] state = new byte[256];

        for (int i = 0; i < 256; i++) {
            state[i] = (byte) i;
        }
        int index1 = 0;
        int index2 = 0;
        if (bKey == null || bKey.length == 0) {
            return null;
        }
        for (int i = 0; i < 256; i++) {
            index2 = ((bKey[index1] & 0xff) + (state[i] & 0xff) + index2) & 0xff;
            byte tmp = state[i];
            state[i] = state[index2];
            state[index2] = tmp;
            index1 = (index1 + 1) % bKey.length;
        }
        return state;
    }

    private static String toHexString(String s) {
        String str = "";
        for (int i = 0; i < s.length(); i++) {
            int ch = (int) s.charAt(i);
            String s4 = Integer.toHexString(ch & 0xFF);
            if (s4.length() == 1) {
                s4 = '0' + s4;
            }
            str = str + s4;
        }
        return str; // 0x表示十六进制
    }

    private static byte[] hexString2Bytes(String src) {
        int size = src.length();
        byte[] ret = new byte[size / 2];
        byte[] tmp = src.getBytes(Charset.defaultCharset());
        for (int i = 0; i < size / 2; i++) {
            ret[i] = uniteBytes(tmp[i * 2], tmp[i * 2 + 1]);
        }
        return ret;
    }

    private static byte uniteBytes(byte src0, byte src1) {
        char b0 = (char) Byte.decode("0x" + new String(new byte[] {src0})).byteValue();
        b0 = (char) (b0 << 4);
        char b1 = (char) Byte.decode("0x" + new String(new byte[] {src1})).byteValue();
        byte ret = (byte) (b0 ^ b1);
        return ret;
    }

    private static byte[] rc4Base(byte[] input, String mKkey) {
        int x1 = 0;
        int y1 = 0;
        byte[] key = initKey(mKkey);
        int xorIndex;
        byte[] result = new byte[input.length];

        for (int i = 0; i < input.length; i++) {
            x1 = (x1 + 1) & 0xff;
            y1 = ((key[x1] & 0xff) + y1) & 0xff;
            byte tmp = key[x1];
            key[x1] = key[y1];
            key[y1] = tmp;
            xorIndex = ((key[x1] & 0xff) + (key[y1] & 0xff)) & 0xff;
            result[i] = (byte) (input[i] ^ key[xorIndex]);
        }
        return result;
    }
}
