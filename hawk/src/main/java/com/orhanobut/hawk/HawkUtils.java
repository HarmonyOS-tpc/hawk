package com.orhanobut.hawk;

/**
 * 工具类
 */
final class HawkUtils {
    private HawkUtils() {
        // no instance
    }

    /**
     * 判空
     * @param message message
     * @param value value
     */
    public static void checkNull(String message, Object value) {
        if (value == null) {
            throw new NullPointerException(message + " should not be null");
        }
    }
    /**
     * 判空
     * @param message message
     * @param value value
     */
    public static void checkNullOrEmpty(String message, String value) {
        if (isEmpty(value)) {
            throw new NullPointerException(message + " should not be null or empty");
        }
    }
    /**
     * 判空
     * @param text text
     * @return boolean
     */
    public static boolean isEmpty(String text) {
        return text == null || text.trim().length() == 0;
    }
}
