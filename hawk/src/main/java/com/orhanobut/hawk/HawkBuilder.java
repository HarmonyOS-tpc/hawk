package com.orhanobut.hawk;

import com.google.gson.Gson;
import com.orhanobut.hawk.Encryption.ConcealEncryption;

import com.orhanobut.hawk.inf.Converter;
import com.orhanobut.hawk.inf.Encryption;
import com.orhanobut.hawk.inf.LogInterceptor;
import com.orhanobut.hawk.inf.Parser;
import com.orhanobut.hawk.inf.Serializer;
import com.orhanobut.hawk.inf.Storage;
import ohos.app.Context;

/**
 * HawkBuilder
 */
public class HawkBuilder {
    /**
     * NEVER ever change STORAGE_TAG_DO_NOT_CHANGE and TAG_INFO.
     * It will break backward compatibility in terms of keeping previous data
     */
    public static final String STORAGE_TAG_DO_NOT_CHANGE = "Hawk2";

    private Context context;
    private Storage cryptoStorage;
    private Converter converter;
    private Parser parser;
    private Encryption encryption;
    private Serializer serializer;
    private LogInterceptor logInterceptor;

    public HawkBuilder(Context context) {
        HawkUtils.checkNull("Context", context);

        this.context = context.getApplicationContext();
    }

    /**
     * setStorage
     * @param storage storage
     * @return HawkBuilder
     */
    public HawkBuilder setStorage(Storage storage) {
        this.cryptoStorage = storage;
        return this;
    }

    /**
     * setParser
     * @param parser parser
     * @return HawkBuilder
     */
    public HawkBuilder setParser(Parser parser) {
        this.parser = parser;
        return this;
    }

    /**
     * setSerializer
     * @param serializer serializer
     * @return HawkBuilder
     */
    public HawkBuilder setSerializer(Serializer serializer) {
        this.serializer = serializer;
        return this;
    }

    /**
     * setLogInterceptor
     * @param logInterceptor logInterceptor
     * @return HawkBuilder
     */
    public HawkBuilder setLogInterceptor(LogInterceptor logInterceptor) {
        this.logInterceptor = logInterceptor;
        return this;
    }

    /**
     * setConverter
     * @param converter converter
     * @return HawkBuilder
     */
    public HawkBuilder setConverter(Converter converter) {
        this.converter = converter;
        return this;
    }

    /**
     * setEncryption
     * @param encryption encryption
     * @return HawkBuilder
     */
    public HawkBuilder setEncryption(Encryption encryption) {
        this.encryption = encryption;
        return this;
    }

    LogInterceptor getLogInterceptor() {
        if (logInterceptor == null) {
            logInterceptor =
                    new LogInterceptor() {
                        @Override
                        public void onLog(String message) {
                            // empty implementation
                        }
                    };
        }
        return logInterceptor;
    }

    Storage getStorage() {
        if (cryptoStorage == null) {
            cryptoStorage = new SharedPreferencesStorage(context, STORAGE_TAG_DO_NOT_CHANGE);
        }
        return cryptoStorage;
    }

    Converter getConverter() {
        if (converter == null) {
            converter = new HawkConverter(getParser());
        }
        return converter;
    }

    Parser getParser() {
        if (parser == null) {
            parser = new GsonParser(new Gson());
        }
        return parser;
    }

    Encryption getEncryption() {
        if (encryption == null) {
            encryption = new ConcealEncryption(context);
        }
        return encryption;
    }

    Serializer getSerializer() {
        if (serializer == null) {
            serializer = new HawkSerializer(getLogInterceptor());
        }
        return serializer;
    }

    /**
     * build
     */
    public void build() {
        Hawk.build(this);
    }
}
